/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal1;

/**
 *
 * @author sairu
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Peem");
        h1.eat();
        h1.walk();
        h1.run();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));

        Animal a1 = h1;
        System.out.println("a1 is land animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal ? " + (a1 instanceof Reptile));
        System.out.println("");

        Cat c1 = new Cat("Win");
        c1.eat();
        c1.walk();
        c1.run();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is land animal ? " + (c1 instanceof LandAnimal));
        Animal a2 = c1;
        System.out.println("a2 is poultry ? " + (a2 instanceof Poultry));
        System.out.println("a2 is aquatic animal ? " + (a2 instanceof AquaticAnimal));
        System.out.println("");

        Dog d1 = new Dog("Tan");
        d1.eat();
        d1.walk();
        d1.run();
        System.out.println("d1 is animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is land animal ? " + (d1 instanceof LandAnimal));
        Animal a3 = d1;
        System.out.println("a3 is reptile ? " + (a3 instanceof Reptile));
        System.out.println("a3 is aquatic animal ? " + (a3 instanceof AquaticAnimal));
        System.out.println("");
        
        Snake s1 = new Snake("Nippon");
        s1.eat();
        s1.sleep();
        s1.crawl();
        System.out.println("s1 is animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is reptile ? " + (s1 instanceof Reptile));
        Animal a4 = s1;
        System.out.println("a4 is reptile ? " + (a4 instanceof Reptile));
        System.out.println("a4 is aquatic animal ? " + (a4 instanceof AquaticAnimal));
        System.out.println("");
        
        Crocodile co1 = new Crocodile("Nat");
        co1.eat();
        co1.sleep();
        co1.crawl();
        System.out.println("co1 is animal ? " + (co1 instanceof Animal));
        System.out.println("co1 is reptile ? " + (co1 instanceof Reptile));
        Animal a5 = co1;
        System.out.println("a5 is land animal ? " + (a5 instanceof LandAnimal));
        System.out.println("a5 is aquatic animal ? " + (a5 instanceof AquaticAnimal));
        System.out.println("");
        
        Bird b1 = new Bird("tee");
        b1.eat();
        b1.fly();
        b1.speak();
        System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is poultry ? " + (b1 instanceof Poultry));
        Animal a6 = b1;
        System.out.println("a6 is poultry ? " + (a6 instanceof Poultry));
        System.out.println("a6 is aquatic animal ? " + (a6 instanceof AquaticAnimal));
        System.out.println("");
        
        Bat bt1 = new Bat("Nan");
        bt1.walk();
        bt1.fly();
        bt1.sleep();
        System.out.println("bt1 is animal ? " + (bt1 instanceof Animal));
        System.out.println("bt1 is poultry ? " + (bt1 instanceof Poultry));
        Animal a7 = bt1;
        System.out.println("a7 is reptile ? " + (a7 instanceof Reptile));
        System.out.println("a7 is land animal ? " + (a7 instanceof LandAnimal));
        System.out.println("");
        
        Fish f1 = new Fish("Oil");
        f1.speak();
        f1.swim();
        f1.sleep();
        System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is aquatic animal ? " + (f1 instanceof AquaticAnimal));
        Animal a8 = f1;
        System.out.println("a8 is reptile ? " + (a8 instanceof Reptile));
        System.out.println("a8 is land animal ? " + (a8 instanceof LandAnimal));
        System.out.println("");
        
        Crab cb1 = new Crab("Fhanglerr");
        cb1.speak();
        cb1.swim();
        cb1.sleep();
        System.out.println("cb1 is animal ? " + (cb1 instanceof Animal));
        System.out.println("cb1 is aquatic animal ? " + (cb1 instanceof AquaticAnimal));
        Animal a9 = cb1;
        System.out.println("a9 is reptile ? " + (a9 instanceof Poultry));
        System.out.println("a9 is land animal ? " + (a9 instanceof LandAnimal));
        System.out.println("");
    }
}
